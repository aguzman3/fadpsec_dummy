#
# Be sure to run `pod lib lint FADPSec.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FADPSec'
  s.version          = '0.0.9'
  s.summary          = 'Modulo para hacer procesos de autenticacion.'

  s.description      = <<-DESC
Modulo para generar procesos de autenticacion combinando procesos como la toma de foto o video.
                       DESC

  s.homepage         = 'www.firmaautografadigital.com'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'aguzman3' => 'aguzman@na-at.com.mx' }
  s.source           = { :git => 'https://gitlab.com/aguzman3/fadpsec_dummy.git', :branch => 'main', :tag => s.version.to_s }

  s.ios.deployment_target = '12.0'
  s.swift_version = '4.2'
  s.static_framework = true
  s.platform     = :ios
  s.requires_arc = true


  #s.source_files = 'FADPSec/Classes/**/*'
  s.vendored_framework  = 'FADPSec.framework'
  
  # s.resource_bundles = {
  #   'FADPSec' => ['FADPSec/Assets/*.png']
  # }

  #s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'


	s.dependency 'Alamofire','5.2'

  s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '$(inherited) -ObjC',
    'ENABLE_BITCODE' => 'NO',
    'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/"'
  }

  s.license = {
      :type => 'Copyright',
      :text => <<-LICENSE
      El presente documento establece los términos y condiciones con los cuales NA-AT Technologies, S.A. de C.V. (“NA-AT”) y sus filiales permite el acceso y uso de su portal de Internet ('https://www.na-at.com.mx') a cualquier persona que ingrese al mismo (“Usuario”). En lo sucesivo los “Términos y Condiciones”.
      
      La visita y/o uso que haga todo Usuario del Portal implica la aceptación de los Términos y Condiciones aquí establecidos, por lo que deberá leer en su totalidad el presente documento.
      
      En el caso de que no esté de acuerdo con los Términos y Condiciones de Uso y Privacidad deberá abstenerse de acceder o utilizar el Portal.
      
      NA-AT Technologies se reserva el derecho de modificar en cualquier tiempo, total o parcialmente, los Términos y Condiciones, sin necesidad de previo aviso.
      
      Los presentes Términos y Condiciones limitan la responsabilidad de NA-AT Technologies y concede al Usuario el uso del Portal de conformidad con lo que aquí se establece, al mismo tiempo que otorga a NA-AT Technologies la propiedad y control respecto de la información que se le proporcione. En el entendido de que la información personal queda sujeta a nuestro Aviso de Privacidad, mismo que aparece publicado en el Portal.
      
      
      
      1.- USO DEL SITIO Y REESTRICCIONES
      A los Usuarios les son aplicables por el simple uso o acceso a cualquiera de las Páginas que integran el Portal de NA-AT Technologies, S.A. de C.V. ('https://www.na-at.com.mx') los términos y condiciones aquí expuestos, por lo que entenderemos que los acepta, y acuerda en obligarse a su cumplimiento.
      
      No se podrá dar un uso distinto al Sitio del señalado en el presente numeral, quedando por tanto prohibido copiar, desplegar, reenviar, reproducir, reutilizar, vender, transmitir, distribuir, bajar, otorgar licencia, modificar, publicar o usar de alguna otra manera los textos, imágenes, datos, gráficas, marcas, logotipos, distintivos y nombres comerciales, pantallas, artículos, software contenidos o utilizados en el Portal y demás materiales e información que aparezcan en el Portal (“Contenido”) para fines públicos, comerciales o para cualquier otro diverso del señalado con anterioridad.
      
      
      2.- PROPIEDAD INTELECTUAL
      Los derechos de propiedad intelectual respecto de los Servicios y Contenidos, los signos distintivos y dominios de las Páginas o el Portal, así como los derechos de uso y explotación de los mismos, incluyendo su divulgación, publicación, reproducción, distribución y transformación, son propiedad exclusiva de NA-AT Technologies y sus derechos se encuentran protegidos por la Ley de la Propiedad Industrial y su Reglamento, la Ley Federal del Derecho de Autor y su Reglamento y por los Tratados Internacionales, y cuya infracción podría derivar en responsabilidad civil y penal. En ningún caso se podrá interpretar que se otorga licencia respecto del Contenido o cualquier derecho de propiedad intelectual, ni se autoriza formar ningún vínculo o hacer referencias del Portal, a menos que se permita mediante convenio que por escrito se celebre con NA-AT Technologies.
      
      
      3.-CONFIDENCIALIDAD
      NA-AT Technologies se obliga a mantener confidencial la información que reciba del Usuario que tenga dicho carácter conforme a las disposiciones legales aplicables, en los Estados Unidos Mexicanos. Toda la información que NA-AT Technologies recabe del Usuario es tratada con absoluta confidencialidad conforme las disposiciones legales aplicables.
      
      Para conocer mayor información de la protección de sus datos personales acuda a la sección "Aviso de Privacidad"
      
      
      4.- JURISDICCIÓN Y LEY APLICABLE
      Para la interpretación, cumplimiento y ejecución de los presentes Términos y Condiciones de Uso y Privacidad, el Usuario está de acuerdo en que serán aplicables las leyes Federales de los Estados Unidos Mexicanos y competentes los tribunales de la Ciudad de México, renunciando expresamente a cualquier otro fuero o jurisdicción que pudiera corresponderles en razón de sus domicilios presentes o futuros o por cualquier otra causa.
      LICENSE
  }

end
